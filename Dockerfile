FROM alpine as base

RUN apk add --update apache2

CMD apache2 -D
